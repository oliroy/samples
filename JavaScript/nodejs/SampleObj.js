'use strict';

module.exports = TkChatServerManager;

function TkChatServerManager(socket, repo) {
    if ( !(this instanceof TkChatServerManager) )
        return new TkChatServerManager();

    //set local
    this._socket = socket || null;
    this._repo = repo || null;
    this.setup();
};


/*
 Setup the initial manager
 */
TkChatServerManager.prototype.setup = function () {
    var self = this;

    self.listen('new-msg');
    self._repo.getLatest(function(err, result) {
        if(!err) self.send('init', result)
        else  self.send('error', err);
    });
}

/*
 Broadcast message back to all those listening to msgType
 */
TkChatServerManager.prototype.sendAll = function(msgType, data) {
    this._socket.emit(msgType, data);
    this._socket.broadcast.emit(msgType, data);
};

/*
 Setup listener for msgType
 */
TkChatServerManager.prototype.listen = function(msgType) {
    var self = this;
    self._socket.on(msgType, function(data) {
        self._repo.save(data, function(savedMsg) {
            self.sendAll(msgType, savedMsg);
        });
    });
};

/*
 Send msg to a single client
 */
TkChatServerManager.prototype.send = function (msgType, data) {
    this._socket.emit(msgType, data);
};


