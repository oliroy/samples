'use strict';

var express = require('express'), chai = require("chai"),sinon = require('sinon'), sinonChai = require("sinon-chai"),
    proxyquire =  require('proxyquire').noCallThru(), expect = chai.expect;

chai.use(sinonChai);

describe('tkChatServerManager', function(){

    var  socket = {}, repo = {}, tkChatServerManager;
    socket.broadcast = {};

    beforeEach(function(){
        tkChatServerManager = require('../lib/tkChatServerManager');
        socket.on = sinon.stub().returns({});
        socket.emit = sinon.stub().returns({});
        socket.broadcast.emit = sinon.stub().returns({});
        socket.on.callsArg(1);
        repo.getLatest = sinon.stub().returns();
    });

    it('should setup tkChatServerManager on creation', function(done){

        repo.save = sinon.stub().returns(done());
        var cs = new tkChatServerManager(socket, repo);

        expect(socket.on).to.have.been.called.once;
        expect(repo.getLatest).to.have.been.called.once;
        expect(repo.save).to.have.been.called.once;
    });


    it('should setup a listener', function(){
        repo.save = sinon.stub();
        repo.save.callsArg(1);

        var cs = new tkChatServerManager(socket, repo);
        var sendSpy = sinon.spy(cs, "send");
        var sendAllSpy = sinon.spy(cs, "sendAll");
        cs.listen('test-type');

        expect(socket.on).to.have.callCount(2);
        expect(repo.save).to.have.callCount(2);
        expect(sendSpy).to.have.callCount(1);
        expect(sendAllSpy).to.have.callCount(1);
    });

    it('should send a message', function(){
        var cs = new tkChatServerManager(socket, repo);

        cs.send();
        expect(socket.emit).to.have.callCount(2);
        expect(socket.broadcast.emit).to.have.callCount(1);
    });


    it('should send a message to all', function(){
        var cs = new tkChatServerManager(socket, repo);

        cs.sendAll();

        expect(socket.emit).to.have.callCount(2);
        expect(socket.broadcast.emit).to.have.callCount(2);
    });
});